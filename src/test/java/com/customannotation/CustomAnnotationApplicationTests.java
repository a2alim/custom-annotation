package com.customannotation;

import com.customannotation.fieldLevel.Color;
import com.customannotation.methodLevel.Level;
import com.customannotation.web.TestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CustomAnnotationApplicationTests {

    @Autowired
    private TestService service;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void testMethodLevelAnnotation() {
        Level demo = new Level();
        demo.setId("12345");
        service.methodLevelAnnotation(demo);
    }

    @Test
    void testFieldLevelAnnotation() {
        Level demo = new Level();
        demo.setId("12345");
        demo.setName("Demo name");
        service.fieldLevelAnnotation(demo);
    }

    @Test
    void testColorValidation() throws Exception {
        Color color = new Color();
        color.setColorName("Hello");
        color.setColorCode("12345");
        mockMvc.perform(post("/color")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new ObjectMapper().writeValueAsString(color)))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
