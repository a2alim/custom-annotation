package com.customannotation.web;

import com.customannotation.fieldLevel.Color;
import com.customannotation.fieldLevel.ColorRepo;
import com.customannotation.methodLevel.CustomLevel;
import com.customannotation.methodLevel.Level;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    private final ColorRepo repo;

    public TestService(ColorRepo repo) {
        this.repo = repo;
    }

    @CustomLevel(msg = "Label msg added")
    void methodLevelAnnotation(Level demo){
        System.out.println("Test method level is called with : "+demo.toString());
    }

    void fieldLevelAnnotation(Level demo){
        System.out.println("Test field level is called with name : "+demo.toString());
    }

    public Color saveColor(Color color) {
        return repo.save(color);
    }

    public Color getColorById(Long id) {
        return repo.findById(id).orElse(null);
    }
}
