package com.customannotation.web;

import com.customannotation.fieldLevel.Color;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    private final TestService service;

    public TestController(TestService service) {
        this.service = service;
    }

    @PostMapping("/colors")
    public Color testColorValidation(@Valid @RequestBody Color color) {
        System.out.println("controller method is called with : " + color.toString());
        return service.saveColor(color);
    }

    @GetMapping("/colors/{id}")
    public Color getColorById(@PathVariable Long id){
        return service.getColorById(id);
    }

}
