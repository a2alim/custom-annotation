package com.customannotation.fieldLevel;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = ColorValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ColorValidation {

    String property();
    Class<?> useClass();
    String message() default "Invalid color: must be RED, GREEN or BLUE";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
