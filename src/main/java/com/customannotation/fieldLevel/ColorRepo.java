package com.customannotation.fieldLevel;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ColorRepo extends JpaRepository<Color, Long> {

    Optional<Color> findByColorName(String name);

}
