package com.customannotation.fieldLevel;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Color {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long Id;
    @ColorValidation(property = "colorName", useClass = Color.class, message = "Color name not acceptable")
    private String colorName;
    private String colorCode;

}
