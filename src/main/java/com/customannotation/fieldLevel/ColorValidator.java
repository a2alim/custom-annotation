package com.customannotation.fieldLevel;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Objects;

public class ColorValidator implements ConstraintValidator<ColorValidation, String> {

    @Autowired
    private ColorRepo colorRepo;
    String property;
    Class<?> useClass;

    @Override
    public void initialize(ColorValidation annotation) {
        ConstraintValidator.super.initialize(annotation);
        property = annotation.property();
        useClass = annotation.useClass();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext cxt) {
        System.out.println("Color validator's valid method call");
        try {
            Color color = null;
            if (Objects.nonNull(colorRepo))
                color = colorRepo.findByColorName(value).orElse(null);
            Field field = useClass.getDeclaredField(property);
            field.setAccessible(true);
            try {
                if (Objects.nonNull(color))
                    field.set(color, value + " added extra");
            } catch (IllegalAccessException e) {
                return false;
            }
        } catch (NoSuchFieldException e) {
            return false;
        }
        return true;
    }
}
