package com.customannotation.methodLevel;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

@Aspect
@Service
public class LabelAspect {

    @Around("@annotation(com.customannotation.methodLevel.CustomLevel)")
    public Object aroundAdvice(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("Around advice is called : "+ pjp.getSignature().getName());

        MethodSignature signature = (MethodSignature) pjp.getSignature();
        CustomLevel annotation = signature.getMethod().getAnnotation(CustomLevel.class);
        String msg = annotation.msg();

        System.out.println("msg : "+ msg);
        Object proceed = pjp.proceed();

        System.out.println("proceed : "+ proceed);
        return proceed;
    }

    @Before("@annotation(com.customannotation.methodLevel.CustomLevel)")
    public void beforeAdvice(JoinPoint joinPoint){
        System.out.println("Before advice is called : "+ joinPoint.getSignature().getName());
    }

    @After("@annotation(com.customannotation.methodLevel.CustomLevel)")
    public void afterAdvice(JoinPoint joinPoint){
        System.out.println("After advice is called : "+ joinPoint.getSignature().getName());
    }


    @Before("@annotation(com.customannotation.fieldLevel.ColorValidation)")
    public void beforeLevelField(JoinPoint joinPoint){
        System.out.println("Before advice is called for LevelField : "+ joinPoint.getSignature().getName());
    }

}
