package com.customannotation.methodLevel;

import lombok.Data;
import lombok.Getter;

@Data
public class Level {

    private String id;

    private String name;
}
